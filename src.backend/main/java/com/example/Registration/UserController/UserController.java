
package com.example.Registration.UserController;
import com.example.Registration.Dto.UserDTO;
import com.example.Registration.Dto.LoginDTO;
import com.example.Registration.Service.UserService;
import com.example.Registration.payload.response.LoginMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
@CrossOrigin(origins="http://localhost:4200/")
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @PostMapping(path = "/register")
    public String saveUser(@RequestBody UserDTO userDTO)
    {
        String id = userService.addUser(userDTO);
        return id; 
    }
    @PostMapping(path = "/login")
    public ResponseEntity<?> loginUser(@RequestBody LoginDTO loginDTO)
    {
        LoginMessage loginResponse = userService.loginUser(loginDTO);
        return ResponseEntity.ok(loginResponse);
    }
}